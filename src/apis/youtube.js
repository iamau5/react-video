import axios from 'axios';


const KEY = 'AIzaSyAeQwICBE0Yeu7RgtpAYWavhkDdV6cvx64';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params:{
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});